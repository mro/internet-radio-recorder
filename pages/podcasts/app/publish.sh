#!/bin/sh
# https://mro.name/radio-privatkopie
cd "$(dirname "${0}")/../" || exit 1

[ "" = "${1}" ] && {
  cat >&2 <<EOF
Refresh the RSS feeds a broadcast belongs to.

Enclosures must have 3-char extensions to be picked up.

SYNOPSIS

  \$ sh ${0} b2/2021/10/05/1905
EOF
  exit 1
}

while [ "" != "${1}" ]
do
  for bc in ./*/"${1}"
  do
    po="$(echo "${bc}" | cut -d / -f 2)"
    # rss mandatory
    sh "${po}/app/rss.sh" "${1}" \
    | xmllint \
      --encode utf-8 \
      --format \
      --nonet \
      --nsclean \
      --output "${po}/broadcasts.rss" \
      --relaxng "app/rss.rng" \
      - \
    &

    # atom optional
    [ -r "${po}/app/atom.sh" ] || continue
    sh "${po}/app/atom.sh" "${1}" \
    | xmllint \
      --encode utf-8 \
      --format \
      --nonet \
      --nsclean \
      --output "${po}/broadcasts.atom" \
      --relaxng "app/atom.rng" \
      - \
    &
  done
  wait

  shift
done 2>&1 | grep -vE "^- validates\$" >&2


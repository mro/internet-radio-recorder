<?xml version="1.0" encoding="UTF-8"?>
<!--
  Turn broadcast xml into one rss item. Called by podcasts/*/app/rss.sh.

 Copyright (c) 2013-2021 Marcus Rohrmoser, https://mro.name/radio-privatkopie

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 MIT License http://opensource.org/licenses/MIT

 http://www.w3.org/TR/xslt/
-->
<xsl:stylesheet
  exclude-result-prefixes="rec"
  extension-element-prefixes="date"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:date="http://exslt.org/date"
  xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
  xmlns:rec="../../../../../assets/2013/radio-pi.rdf"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

  <xsl:output
    indent="yes"
    method="xml"
    omit-xml-declaration="yes"
    />

  <xsl:template match="/">
    <xsl:for-each select="rec:broadcast">
      <xsl:variable name="id" select="rec:meta[@name='DC.identifier']/@content"/>
      <item>
        <title><xsl:value-of select="rec:meta[@name='DC.title']/@content"/></title>
        <link><xsl:value-of select="$baseurl"/>/stations/<xsl:value-of select="$id"/>.xml</link>
        <guid isPermaLink="true"><xsl:value-of select="$podcast"/>/<xsl:value-of select="$id"/></guid>
        <description><xsl:value-of select="rec:meta[@name='DC.description']/@content"/></description>
        <pubDate><xsl:value-of select="$pubDate"/></pubDate>
        <itunes:explicit>false</itunes:explicit>
        <enclosure type="{$mimetype}" url="{$enclosure}?rss" length="{$filesize}"/>
      </item>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>

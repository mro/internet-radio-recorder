#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/.." || exit 1

[ "${1}" = "" ] && {
cat >&2 <<EOF
Write a podcast marker next the broadcast into /stations/*/????/??/??/????.podcasts

SYNOPSIS

  \$ sh ${0} b2/2021/10/05/1905
EOF
  exit 1
}

while [ "" != "${1}" ]
do
  ls "../stations/${1}.xml" >/dev/null || exit $?

  # it's a bit hairy to write inside the stations dir.
  dst="../stations/${1}.podcasts"
  {
    readonly pre="../../../../.."
    for po in "../podcasts"/*/"${1}"
    do
      [ -r "${po}" ] && echo "${pre}/$(echo "${po}" | cut -d / -f 2-3)"
    done
  } | sort > "${dst}"

  [ -s "${dst}" ] || rm "${dst}"

  shift
done


#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/.." || exit 1
set -e

ls "../stations/${1}.xml" >/dev/null || exit 1

do_touch () {
  mkdir -p "$(dirname "${1}")" \
    && touch "${1}"
}

for mat in */app/match.sh
do
  sh "${mat}" "${1}" || continue

  # if it does match:
  name="$(dirname "$(dirname "${mat}")")"
  do_touch "${name}/${1}"
  do_touch "../enclosures/${1}.reserved"
done


<?xml version="1.0" encoding="UTF-8"?>
<!--
  Turn broadcast xml into one atom entry. Called by podcasts/*/app/atom.sh.

 Copyright (c) 2021-2021 Marcus Rohrmoser, https://mro.name/radio-privatkopie

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 MIT License http://opensource.org/licenses/MIT

 http://www.w3.org/TR/xslt/
-->
<xsl:stylesheet
  exclude-result-prefixes="rec"
  extension-element-prefixes="date"
  xmlns="http://www.w3.org/2005/Atom"
  xmlns:date="http://exslt.org/date"
  xmlns:rec="../../../../../assets/2013/radio-pi.rdf"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

  <xsl:output
    indent="yes"
    method="xml"
    omit-xml-declaration="yes"
    />

  <xsl:template match="/">
    <xsl:for-each select="rec:broadcast">
      <xsl:variable name="id" select="rec:meta[@name='DC.identifier']/@content"/>
      <entry>
        <title><xsl:value-of select="rec:meta[@name='DC.title']/@content"/></title>
        <id><xsl:value-of select="$podcast"/>/<xsl:value-of select="$id"/></id>
        <updated><xsl:value-of select="$updated"/></updated>
        <content><xsl:value-of select="rec:meta[@name='DC.description']/@content"/></content>
        <link rel="enclosure" length="{$filesize}" href="{$baseurl}/enclosures/{$id}.mp3?atom" type="audio/mpeg"/>
      </entry>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>


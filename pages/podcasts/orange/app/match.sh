#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/.." || exit 1

readonly bc="../../stations/${1}.xml"
readonly YES=0
readonly NO=1

grep -E 'Jazz &amp; Politik.+ name="DC\.title"' \
  "${bc}" > /dev/null \
&& exit $YES

grep -E 'Bairisch Esperanto.+ name="DC\.title\.episode"' \
  "${bc}" > /dev/null \
&& exit $YES

grep -E 'Wiederholung von 15\.05 Uhr.+ name="DC\.description"' \
 "${bc}" > /dev/null \
&& exit $NO

grep -iE 'radioSpitzen.+ name="DC\.title"' \
  "${bc}" > /dev/null \
&& exit $YES


grep -E 'Wiederholung (von|vom Freitag,) 16\.05 Uhr.+ name="DC\.description"' \
  "${bc}" > /dev/null \
&& exit $NO

grep -E 'Eins zu Eins. Der Talk.+ name="DC\.title"' \
  "${bc}" > /dev/null \
&& grep -E 'Bogdahn.+ name="DC\.description"' \
  "${bc}" > /dev/null \
&& exit $YES


grep -E 'Urbi et Orbi.+ name="DC\.title"' \
  "${bc}" > /dev/null \
&& exit $YES

grep -E 'Resetarits' \
  "${bc}" > /dev/null \
&& exit $YES

exit $NO


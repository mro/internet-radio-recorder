#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/.." || exit 1

readonly bc="../../stations/${1}.xml"
readonly YES=0
readonly NO=1

grep -F ' name="DC.description"' "${bc}" \
| grep -iF 'Ellis Kaut' \
| grep -iF 'Hans Clarin' \
> /dev/null \
&& exit $YES

exit $NO


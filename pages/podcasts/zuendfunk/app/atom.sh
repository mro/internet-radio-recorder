#!/bin/sh
# https://mro.name/radio-privatkopie
#
readonly baseurl="http://rec.mro.name"

cd "$(dirname "${0}")/.." || exit 1

readonly stations="../../stations"
readonly enclosures="../../enclosures"

readonly rfc3339='%FT%T%z'
date_now_rfc3339 () {
  date +"${rfc3339}" | sed -e 's/\(..\)$/:\1/'
}
date_file_rfc3339 () {
  date -r "${1}" +"${rfc3339}" | sed -e 's/\(..\)$/:\1/'
}

readonly self="${baseurl}/podcasts/$(basename "$(pwd)")"
cat <<EOF
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="de">
  <title>🔥 Zündfunk</title>
  <link href="${self}.atom" rel="self" type="application/atom+xml" />
  <id>${self}</id>
  <updated>$(date_now_rfc3339)</updated>
  <generator uri="https://mro.name/radio-privatkopie" version="2.0">Internet Radio Recorder</generator>
  <author>
    <name>https://mro.name/radio-privatkopie</name>
  </author>
EOF

for id in $(ls --reverse */????/??/??/???? | head -n 50)
do
  [ -r "${enclosures}/${id}.mp3" ] || continue

  xsltproc \
    --stringparam baseurl "${baseurl}" \
    --stringparam filesize "$(wc -c < "${enclosures}/${id}.mp3")" \
    --stringparam podcast "${self}" \
    --stringparam updated "$(date_file_rfc3339 "${enclosures}/${id}.mp3")" \
    ../app/broadcast2atom.xslt "${stations}/${id}.xml" \
    2>/dev/null
done

cat <<EOF
</feed>
EOF


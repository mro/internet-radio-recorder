#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/.." || exit 1

readonly bc="../../stations/${1}.xml"
readonly YES=0
readonly NO=1

grep -E 'radioTexte.+ name="DC\.title"' \
  "${bc}" > /dev/null \
&& exit $YES

grep -E 'Der Dieb und die Hunde.+ name="DC\.title\.episode"' \
  "${bc}" > /dev/null \
&& exit $YES

exit $NO


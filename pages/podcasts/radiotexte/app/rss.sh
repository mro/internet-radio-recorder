#!/bin/sh
# https://mro.name/radio-privatkopie
#
readonly baseurl="http://rec.mro.name"

cd "$(dirname "${0}")/.." || exit 1

readonly stations="../../stations"
readonly enclosures="../../enclosures"

readonly rfc822='%a, %d %b %Y %T %z'
date_now_rfc822 () {
  LANG=C date +"${rfc822}"
}
date_file_rfc822 () {
  LANG=C date -r "${1}" +"${rfc822}"
}

readonly self="${baseurl}/podcasts/$(basename "$(pwd)")"
cat <<EOF
<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <link>${self}</link>
    <!-- validate https://validator.w3.org/feed/check.cgi?url=${self}.rss -->
    <atom:link href="${self}.rss" rel="self" type="application/rss+xml" />
    <title>📖Radiotexte</title>
    <itunes:subtitle>📖 🎙</itunes:subtitle>
    <description>🔥 https://mro.name/radio-privatkopie</description>
    <itunes:summary>Hörstoff</itunes:summary>
    <itunes:category text="Society &amp; Culture"/>
    <itunes:owner>
      <itunes:name>Your Name</itunes:name>
      <itunes:email>your-email@example.com</itunes:email>
    </itunes:owner>
    <itunes:explicit>false</itunes:explicit>
    <language>de</language>
    <lastBuildDate>$(date_now_rfc822)</lastBuildDate>
    <pubDate>$(date_now_rfc822)</pubDate>
EOF

for id in $(ls -t */????/??/??/???? | head -n 50)
do
  encfi="$(ls "${enclosures}/${id}".??? 2>/dev/null)"
  [ "" = "${encfi}" ] && continue

  xsltproc \
    --stringparam baseurl   "${baseurl}" \
    --stringparam enclosure "${baseurl}/foo/bar/${encfi}" \
    --stringparam filesize  "$(wc -c < "${encfi}")" \
    --stringparam mimetype  "$(file --brief --mime-type "${encfi}")" \
    --stringparam podcast   "${self}" \
    --stringparam pubDate   "$(date_file_rfc822 "${encfi}")" \
    ../app/broadcast2rss.xslt "${stations}/${id}.xml" \
    2>/dev/null
done

cat <<EOF
  </channel>
</rss>
EOF


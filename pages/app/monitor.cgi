#!/bin/sh
# https://mro.name/radio-privatkopie
#
# checked hourly, see e.g. https://updown.io/44q5 join https://updown.io/r/c7a7S
# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -eu
cd "$(dirname "${0}")/../" || exit 1

cr="\\n"
# some nasty but fancy utf-8 emojis
readonly ok="✅"
readonly warn="⚠️"
readonly fail="❌"

msg=""

error_exit () {
  cat <<EOF
Status: ${1}
Content-Type: text/plain; charset=utf-8

https://mro.name/radio-privatkopie
EOF
  echo "${msg}${cr}${2}"
  exit 1
}

readonly cmd0="stations/b2/app/html2broadcast-br-$(uname -s)-$(uname -m)"

jq --version 1>/dev/null \
  || error_exit 500 "${fail} please install \$ sudo apt-get install jq"
  msg="${msg}${cr}${ok} jq"
curl --version >/dev/null \
  || error_exit 500 "${fail} please install \$ sudo apt-get install curl"
  msg="${msg}${cr}${ok} curl"
id3v2 --version 2>/dev/null >&2 \
  || error_exit 500 "${fail} please install \$ sudo apt-get install id3v2"
  msg="${msg}${cr}${ok} id3v2"
ffmpeg -version 2>/dev/null >&2 \
  || error_exit 500 "${fail} please install \$ sudo apt-get install ffmpeg"
  msg="${msg}${cr}${ok} ffmpeg"
# which ffmpeg 2> /dev/null >&2 \
#   || error_exit 500 "${fail} please install \$ sudo apt-get install ffmpeg"
#   msg="${msg}${cr}${ok} ffmpeg"
xmllint --version 2>/dev/null \
  || error_exit 500 "${fail} please install \$ sudo apt-get install libxml2-utils"
  msg="${msg}${cr}${ok} xmllint"
xsltproc --version 2>/dev/null >&2 \
  || error_exit 500 "${fail} please install \$ sudo apt-get install xsltproc"
  msg="${msg}${cr}${ok} xsltproc"
[ "radio-pi www-data" = "$(id -Gn radio-pi)" ] \
  || error_exit 500 "${fail} please setup \$ sudo useradd -mG www-data radio-pi"
  msg="${msg}${cr}${ok} user radio-pi"
txt="$(/usr/sbin/service atd status)"
[ "atd is running." = "${txt}" ] \
  || error_exit 500 "${fail} please install dma and \$ sudo apt-get instal at"
  msg="${msg}${cr}${ok} ${txt}"
txt="$(/usr/sbin/service cron status)"
[ "cron is running." = "${txt}" ] \
  || error_exit 500 "${fail} please install \$ sudo apt-get install cron"
  msg="${msg}${cr}${ok} ${txt}"
txt="$(/usr/sbin/service lighttpd status)"
[ "lighttpd is running." = "${txt}" ] \
  || error_exit 500 "${fail} please install a webserver, e.g. \$ sudo apt-get install lighttpd"
  msg="${msg}${cr}${ok} ${txt}"
txt="$(/usr/sbin/service ntp status)"
[ "NTP server is running." = "${txt}" ] \
  || error_exit 500 "${fail} ${txt}"
  msg="${msg}${cr}${ok} ${txt}"
[ ! -s "error.log" ] \
  || error_exit 500 "${fail} please inspect error.log - there are some."
  msg="${msg}${cr}${ok} error.log empty"
[ "error.log" = "$(find error.log -mmin -70)" ] \
  || error_exit 500 "${fail} error.log is old."
  msg="${msg}${cr}${ok} error.log fresh"
"${cmd0}" --version 2>/dev/null >&2 \
  || error_exit 500 "${fail} please install \$ ${cmd0}"
  msg="${msg}${cr}${ok} ${cmd0}"

if [ 0 = 1 ] ; then
if [ "" = "${REQUEST_SCHEME}" ] ; then
  base="http://rec.mro.name"
else
  base="${REQUEST_SCHEME}://${HTTP_HOST}:${SERVER_PORT}"
fi
for sta in stations/b2/now
do
  url="${base}/${sta}"
  if [ 0 = 1 ] && \
    [ "${sta}" = "stations/b5/now" ] 
  then
    msg="${msg}${cr}${warn} GET ${url}"
    continue
  fi

  code="$(curl \
    --head \
    --location \
    --output /dev/null \
    --silent \
    --url "${url}" \
    --user-agent "https://mro.name/radio-privatkopie" \
    --write-out "%{http_code}" \
    2>/dev/null)"
  [ "${code}" = "200" ] || error_exit 500 "${fail} status ${code} <= GET ${url}"
  msg="${msg}${cr}${ok} GET ${url}"
done
fi

cat <<EOF
Status: 200
Content-Type: text/plain; charset=utf-8

https://mro.name/radio-privatkopie
EOF
echo "${msg}"


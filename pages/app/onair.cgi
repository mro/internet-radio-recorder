#!/bin/sh
# https://mro.name/radio-privatkopie
#
# redirect to the given broadcast according station and time.
#
# Expected URLs:
#
# http://rec.mro.name/app/onair.cgi/stations/b2?t=2021-10-10T17:05:00+02:00
# http://rec.mro.name/stations/b2/now?t=2021-10-10T17:05:00+02:00
#
# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -eu
[ "" = "${QUERY_STRING}" ] && QUERY_STRING="t=$(date +'%FT%T%z' | sed -e 's/\(..\)$/:\1/')"

case "${QUERY_STRING}" in
  t=*) readonly t0="$(echo "${QUERY_STRING}" | cut -c 3-)"
    ;;
  *)  echo miss >> /tmp/onair.log
    exit 1
    ;;
esac

[ "" = "${PATH_INFO}" ] && {
cat <<EOF
Content-Type: text/xml; charset=utf-8

<broadcasts xmlns="../../../../../assets/2013/radio-pi.rdf" xml:lang="de" modified="1970-01-01T00:00:01+00:00">
  <!-- empty at the time but should be each stations the currently airing broadcast -->
</broadcasts>
EOF
  exit 0
}

cd "..${PATH_INFO}" || exit 1
# we're inside stations/<station>
# find a the biggest match <= 'now'
readonly dir="$(date -d "${t0}" +'%Y/%m/%d')"
readonly sto="$(date -d "${t0}" +'%H%M')"
for bc_str in $(ls --reverse "${dir}"/????.xml)
do
  bc="$(basename "${bc_str}" .xml)"
  [ "${bc}" -le "${sto}" ] && {
    cat <<EOF
Status: 302
Location: ../../../..${PATH_INFO}/${bc_str}
Content-Type: text/plain; charset=utf-8

Redirecting to the according broadcast…
EOF
    exit 0
  }
done

# none found, prbly far future.
cat <<EOF
Status: 404
Content-Type: text/plain; charset=utf-8

Not Found
EOF


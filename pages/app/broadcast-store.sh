#!/bin/sh
# https://mro.name/radio-privatkopie
#
# validate and store a single broadcast from stdin and process podcast matches etc.
# ${1} is used just for the error message in case of failure, so e.g. an original
# url.
#
cd "$(dirname "${0}")" || exit 1

readonly tmp="../stations/.bc-$$.xml~"

{
  {
    echo "<?xml-stylesheet type='text/xsl' href='../../../app/broadcast2html.xslt'?>"
    cat -
  } \
  | xmllint \
    --encode "utf-8" \
    --format \
    --nonet \
    --nowarning \
    --nsclean \
    --output "${tmp}" \
    --relaxng "../app/pbmi2003-recmod2012/broadcast.rng" \
    - \
  || {
    echo "failed to scrape ${1}" >&2 
    exit 1
  }
} 2>&1 \
| grep -vE -- \
  "^- validates\$" \
>&2

readonly id="$(grep -F ' name="DC.identifier"/>' "${tmp}" | cut -d '"' -f 2)"
readonly mod="$(grep -hoE ' modified="[^"]+"'    "${tmp}" | cut -d '"' -f 2)"
readonly dst="../stations/${id}.xml"

touch -d "$(echo ${mod} | cut -c 1-19)" "${tmp}"
mkdir -p "$(dirname "${dst}")"
mv "${tmp}" "${dst}"

sh "../podcasts/app/match.sh" "${id}"
# this may have to be done outside the loop to avoid races?
[ -r "../enclosures/${id}.reserved" ] \
  && sh "../enclosures/app/schedule.sh" "${id}"

echo "<${id}> <http://purl.org/dc/terms/modified> \"${mod}\" ." \
  >> "../stations/modified.ttl"

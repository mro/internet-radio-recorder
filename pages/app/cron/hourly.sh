#!/bin/sh
# https://mro.name/radio-privatkopie
#
cd "$(dirname "${0}")/../../stations/" || exit 1

for st in ./*/app/scrape.sh
do
  sh "${st}" 2>> ../error.log &
done

wait

touch ../error.log


#!/bin/sh
# https://mro.name/radio-privatkopie
#
# dash man page https://www.mankier.com/1/dash#Description-Builtins
#
cd "$(dirname "${0}")" || exit 1

readonly base="../.."

# no tombstones.
find "${base}/enclosures" \
  -mindepth 5 \
  -type f \
  -mtime +91 \
  -delete
find "${base}/enclosures" \
  -type d \
  -empty \
  -delete

per_station () {
  . ./app/etc.sh

  curl \
    --output logo.svg \
    --remote-time \
    --silent \
    --time-cond logo.svg \
    --url "${LOGO_URL}" \
    --user-agent "https://mro.name/radio-privatkopie"

  [ app/etc.sh -ot about.rdf ] || {
    export LOGO_URL PROGRAM_URL STREAM_URL
    envsubst < "${base}/app/about.rdf.env" \
    | xmllint \
      --encode utf-8 \
      --format \
      --nsclean \
      --output about.rdf \
      -
  }
}

readonly cwd="$(pwd)"
for etc in "${base}/stations"/*/"app/etc.sh"
do
  cd "${cwd}" || continue
  cd "$(dirname "${etc}")/.." || continue

  per_station
done


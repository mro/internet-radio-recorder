#!/bin/sh
#
# https://stackoverflow.com/a/27595805/349514

# if we are friday and hour = 18

# [ 185 = $(date +%H%u) ] || exit 0

cat <<EOF
<!-- unorthodox relative namespace to enable http://www.w3.org/TR/grddl-tests/#sq2 without a central server -->
<broadcast xmlns="../../../../../assets/2013/radio-pi.rdf" xml:lang="de" modified="2021-10-15T17:04:11+02:00">
 <meta content="ro-galaxy/$(date -d $(date -d '1 hour' '+%H:00:00') '+%Y/%m/%d/%H00')" name="DC.identifier"/>
 <meta content="/app/pbmi2003-recmod2012/" name="DC.scheme"/>
 <meta content="de" name="DC.language"/>
 <meta content="GalaxyMixed" name="DC.title"/>
 <meta content="https://www.galaxymixed.de/ruhestoerung/" name="DC.subject"/>
 <meta content="$(date -d $(date -d '1 hour' '+%H:00:00') '+%FT%T%z' | sed "s/\(..\)$/:\1/")" name="DC.format.timestart"/>
 <meta content="$(date -d $(date -d '2 hours' '+%H:00:00') '+%FT%T%z' | sed "s/\(..\)$/:\1/")" name="DC.format.timeend"/>
 <meta content="PT60M" name="DC.format.duration"/>
 <!-- meta content="https://www.br.de/radio/br24-programmfahne-nachrichten-100~_v-img__16__9__xl_-d31c35f8186ebeb80b0cd843a7c267a0e0c81647.jpg?version=e37d8" name="DC.image"/ -->
 <meta content="http://www.galaxymixed.de/" name="DC.author"/>
 <meta content="" name="DC.source"/>
</broadcast>
EOF


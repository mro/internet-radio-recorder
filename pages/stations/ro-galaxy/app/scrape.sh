#!/bin/sh
# http://mro.name/radio-privatkopie
#
# https://stackoverflow.com/a/27595805/349514

# only if we are monday and hour = 18
# [ 185 = $(date +%H%u) ] || exit 0
[ 18 = $(date +%H) ] || exit 0

cd "$(dirname "${0}")"

readonly base="../../.."

sh date.sh \
  | sh "${base}/app/broadcast-store.sh" "computed fake"


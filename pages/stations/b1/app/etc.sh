
# https://mro.name/radio-privatkopie

PROGRAM_URL="https://www.br.de/radio/bayern1/programmkalender/bayern-eins114~json.json"
# https://www.br.de/service/urls-livestreams-100.html
STREAM_URL="https://dispatcher.rndfnk.com/br/br1/obb/mp3/mid"

LOGO_URL="https://upload.wikimedia.org/wikipedia/commons/f/f8/Bayern1-logo.svg"


# https://mro.name/radio-privatkopie

PROGRAM_URL="https://www.br.de/radio/br-heimat/programmkalender/br-heimat-116~json.json"
# https://www.br.de/service/urls-livestreams-100.html
STREAM_URL="https://dispatcher.rndfnk.com/br/brheimat/live/mp3/mid"

LOGO_URL="https://upload.wikimedia.org/wikipedia/commons/d/d0/BR_Heimat_Logo.svg"


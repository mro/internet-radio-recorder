
# https://mro.name/radio-privatkopie

PROGRAM_URL="https://www.br.de/puls/programm/puls-radio/programmkalender/programmfahne104~json.json"
# https://www.br.de/service/urls-livestreams-100.html
STREAM_URL="https://dispatcher.rndfnk.com/br/puls/live/mp3/mid"

LOGO_URL="https://upload.wikimedia.org/wikipedia/commons/e/e7/BR_puls_Logo.svg"


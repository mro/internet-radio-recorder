
# https://mro.name/radio-privatkopie

PROGRAM_URL="https://www.br.de/radio/br-schlager/programmkalender/br-schlager-112~json.json"
# https://www.br.de/service/urls-livestreams-100.html
STREAM_URL="https://dispatcher.rndfnk.com/br/brschlager/live/mp3/mid"

LOGO_URL="https://upload.wikimedia.org/wikipedia/de/e/e3/Bayern_plus.svg"


# https://mro.name/radio-privatkopie

PROGRAM_URL="https://www.br.de/radio/bayern2/programmkalender/programmfahne102~json.json"
# br.de/internetradio
STREAM_URL="https://dispatcher.rndfnk.com/br/br2/live/mp3/mid"

LOGO_URL="https://upload.wikimedia.org/wikipedia/commons/8/8a/Bayern_2_Logo_%282007-present%29.svg"

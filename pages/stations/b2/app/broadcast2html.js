
// https://gist.github.com/pjdietz/e0545332e2fc67a9a460
function rfc3339(d) {
  function pad(n) {
    return n < 10 ? "0" + n : n;
  }

  function timezoneOffset(offset) {
    if (offset === 0) {
        return "Z";
    }
    let sign = (offset > 0) ? "-" : "+";
    offset = Math.abs(offset);
    return sign + pad(Math.floor(offset / 60)) + ":" + pad(offset % 60);
  }

  return d.getFullYear() + "-" +
    pad(d.getMonth() + 1) + "-" +
    pad(d.getDate()) + "T" +
    pad(d.getHours()) + ":" +
    pad(d.getMinutes()) + ":" +
    pad(d.getSeconds()) +
    timezoneOffset(d.getTimezoneOffset());
}

function human_datetime(dat) {
  let weekdaynames = [ "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa" ];
  let monthnames = [ "Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" ];

  function pad(n) {
    return n < 10 ? "0" + n : n;
  }

  var ret = '';
  ret += weekdaynames[dat.getDay()] + ", ";
  ret += dat.getDate() + ". ";
  ret += monthnames[dat.getMonth()] + " ";
  ret += dat.getFullYear() + " ";
  ret += pad(dat.getHours()) + ":";
  ret += pad(dat.getMinutes());
  return ret;
}

function url_norm(url) {
  let n = url.replace(/\/[^/]+\/\.\.\//, '/');
  return n == url
    ? url
    : url_norm(n);
}

function amend_podcasts (ok, xhr, p) {
  if (! ok) {
    return;
  }

  let hrefs = xhr.responseText.trim().split(/\s+/m);
  if (hrefs.length < 1)
    return;

  let base = p.ownerDocument.location;
  let assets = "../../../../../assets";
  var html = "";
  for(let i = 0; i < hrefs.length; i++) {
    let href = hrefs[i];
    let feed = url_norm(base + "/../" + href + ".rss");
    let lbl = href.replace('../../../../../podcasts/', '');
    html += "<a href='" + href + "'>" + lbl + "</a> ";
    html += "<a href='" + feed + "' rel='alternate' type='application/rss+xml'><img height='16' alt='feed' src='" + assets + "/Feed-icon.svg'/></a> ";
    html += "<a href='https://validator.w3.org/feed/check.cgi?url=" + feed + "'><img height='16' alt='Valid RSS Badge' src='" + assets + "/valid-rss-rogers.png'/></a> ";
    html += "<br class='br'/> ";
  }
  p.innerHTML = html;
}

function amend_canonical_url(url) {
  {
    let col = document.getElementsByClassName("canonical-url");
    for (let i = 0; i < col.length; i++) {
      col[i].innerText = url;
    }
  }
  {
    let col = document.getElementsByClassName("base-url");
    for (let i = 0; i < col.length; i++) {
      col[i].innerText = url + '/../../../../../..';
    }
  }
}

function id(_id) {
  return document.getElementById(_id);
}

function amend_back_forth_hrefs (timestart, timeend) {
  function href(t0, m) {
    return '../../../now?t=' + rfc3339(new Date(t0.getTime() + m * 60 * 1000));
  }
  id('prevP1W' ).href = href(timestart, -60 * 24 * 7);
  id('prevP1D' ).href = href(timestart, -60 * 24);
  id('prevPT1M').href = href(timestart, -1);
  id('nextPT1M').href = href(timeend,    1);
  id('nextP1D' ).href = href(timestart,  60 * 24);
  id('nextP1W' ).href = href(timestart,  60 * 24 * 7);
}

window.addEventListener('DOMContentLoaded', (event) => {
  let timestart = new Date(id('dtstart').dataset.rfc3339);
  let timeend   = new Date(id('dtend'  ).dataset.rfc3339);
  let now       = new Date();

  id('dtstart').innerText = human_datetime(timestart);
  amend_back_forth_hrefs(timestart, timeend);
  amend_canonical_url(window.location.href);

  {
    var clz = 'is_past';
    if( now < timestart )
      clz = 'is_future';
    else if( now < timeend )
      clz = 'is_current';
    document.getElementsByTagName('html')[0].classList.add(clz);
  }

  {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== 4) return;
      amend_podcasts(xhr.status >= 200 && xhr.status < 300, xhr, id('podcasts'));
    };
    xhr.open('GET', document.location.href.replace('.xml', '.podcasts'));
    xhr.send();
  }

  function finishAlldayCurrentEntry(a) {
    // a.removeClass('is_past').addClass('is_current').append( jQuery('<span/>').text('jetzt') );
     a.classList.remove('is_past');
     a.classList.add('is_current');
     let span = document.createElement('span');
     span.textContent = 'jetzt';
     a.appendChild(span);
  }

  console.log('add other broadcasts of the day (same station)');
  let xhr2 = new XMLHttpRequest();
  xhr2.onload = function() {
    // console.log('GET ' + xhr2.responseURL + ' ' + xhr2.status);
    if (xhr2.status < 200 || xhr2.status >= 400)
      return;

    let parent = document.getElementById('allday');
    parent.removeChild(parent.firstChild);

    var hasRecording = false;
    var pastBC = null;
    let allA = new DOMParser().parseFromString(xhr2.responseText, 'text/html').getElementsByTagName('a');
    for(let i = 0; i < allA.length; i++) {
      let src = allA[i];
      let href = src.getAttribute('href');
      let me = document.createElement('a');
      me.textContent = src.textContent;
      let li = document.createElement('li');
      li.appendChild(me);

      // console.log(i + ' ' + href);
      if( '../' === href )                  // ignore parent link
        continue;
      if( hasRecording )                              // previous entry was a .podcasts recording marker
        me.classList.add('has_podcast');
      if( hasRecording = href.endsWith('.podcasts') ) // remember and swallow .podcasts
        continue;
      let txt = me.textContent.replace(/\.xml$/i, '');
      let ma = txt.match(/^(\d{2})(\d{2})$/);
      if( false && ma ) {
        let t0 = dtstart.hours(ma[1]).minutes(ma[2]).seconds(0); // assumes same day
        me.getAttribute('title', t0.format());
        me.textContent = t0.format('HH:mm') + ' ' + ma[3];
        // set past/current/future class
        if( now < t0 ) {
          if(pastBC) {
            finishAlldayCurrentEntry(pastBC);
            pastBC = null;
          }
          me.classList.add('is_future');
        } else {
          pastBC = me;
          me.classList.add('is_past');
        }
      } else {
        me.textContent = txt;                                 // index usually.
      }
      me.setAttribute('href', href );
      parent.appendChild(li);
    }
    if( pastBC && now < dtstart.hours(24).minutes(0).seconds(0) )
      finishAlldayCurrentEntry(pastBC)
    parent.setAttribute('style', 'display:block');
  }
  xhr2.open('GET', '.');
  xhr2.send();
});


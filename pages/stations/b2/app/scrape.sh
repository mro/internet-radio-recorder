#!/bin/sh
# https://mro.name/radio-privatkopie
#
# Scrape broadcasts on air the next 90 minutes.
#
# Calls ../../../app/broadcast-store.sh to write file, match podcasts etc.
#
# Called from ../../../app/cron/hourly.sh
#
cd "$(dirname "${0}")" || exit 1

readonly cmd="./html2broadcast-br-$(uname -s)-$(uname -m)"

readonly tmin="$(date +'%FT%T')"
if [ "12:30" = "$(date -d '11:00 today + 90 minutes' +'%H:%M' 2>/dev/null)" ] ; then
  # looks like a Linux
  readonly tmax="$(date -d 'now + 120 minutes' +'%FT%T')"
else
  # hope it's a BSD
  readonly tmax="$(date -v+120M +'%FT%T')"
fi

. ./etc.sh

for ur in $(curl \
  --connect-timeout 5 \
  --location \
  --silent \
  --url "${PROGRAM_URL}" \
  --user-agent "https://mro.name/radio-privatkopie" \
| jq ".channelBroadcasts[] | select(\"${tmin}\" <= .broadcastStartDate and .broadcastStartDate <= \"${tmax}\") | .broadcastHtml" \
| grep -hoE '/[^" ]+/programmkalender/ausstrahlung-[0-9]+\.html' )
do
  url="https://br.de${ur}"
  # echo "GET ${url}"

  tmp_html="$(mktemp /tmp/radio-scrape.XXXXXX)"
  trap "rm -f ${tmp_html}" EXIT INT HUP QUIT TERM ALRM USR1
  curl \
    --connect-timeout 10 \
    --location \
    --output "${tmp_html}" \
    --silent \
    --url "${url}" \
    --user-agent "https://mro.name/radio-privatkopie" \
  || {
    echo "failed: GET ${url}"
    rm -f "${tmp_html}"
    continue
  }

  "${cmd}" < "${tmp_html}" \
  | sh "../../../app/broadcast-store.sh" "${url}"
  rm -f "${tmp_html}"
done

wait


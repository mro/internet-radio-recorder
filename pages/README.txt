https://mro.name/radio-privatkopie


______          _ _        ______     _            _   _               _      
| ___ \        | (_)       | ___ \   (_)          | | | |             (_)     
| |_/ /__ _  __| |_  ___   | |_/ / __ ___   ____ _| |_| | _____  _ __  _  ___ 
|    // _` |/ _` | |/ _ \  |  __/ '__| \ \ / / _` | __| |/ / _ \| '_ \| |/ _ \
| |\ \ (_| | (_| | | (_) | | |  | |  | |\ V / (_| | |_|   < (_) | |_) | |  __/
\_| \_\__,_|\__,_|_|\___/  \_|  |_|  |_| \_/ \__,_|\__|_|\_\___/| .__/|_|\___|
                                                                | |           
                                                                |_|           


Automatically record radio broadcasts and make rss feeds for private use.

Cron-triggered:

  $ sh app/cron/hourly.sh

#!/bin/sh
# https://mro.name/radio-privatkopie
#

# https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
set -eu
[ "POST" = "${REQUEST_METHOD}" ] || {
cat <<EOF
Status: 405
Content-Type: text/plain; charset=utf-8

Method not allowed.

https://mro.name/radio-privatkopie
EOF
   exit 0
}


cd "$(dirname "${0}")" || exit 1
readonly id="$(echo "${HTTP_REFERER}" | grep -hoE "/stations/[^/]+/[0-9]{4}/[0-9]{2}/[0-9]{2}/[0-9]{4}" | cut -d / -f 3-)"

sudo -u radio-pi sh schedule.sh "${id}" \
  || exit 1

cat <<EOF
Status: 302
Content-Type: text/plain; charset=utf-8
Location: ../../stations/${id}.xml

Das war aber einfach.
EOF


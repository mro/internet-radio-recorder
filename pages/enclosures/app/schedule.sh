#!/bin/sh
# https://mro.name/radio-privatkopie
#
set -e
cd "$(dirname "${0}")/.." || exit 1

[ -r "${1}.pending" ] && exit 0
[ -r "${1}.ripping" ] && exit 0
ls "${1}".??? >/dev/null 2>&1 && exit 0

readonly bc="../stations/${1}.xml"
readonly tmin_iso="$(grep -F " name=\"DC.format.timestart\"" "${bc}" | cut -d '"' -f 2)"
readonly tmin_epo="$(date -d "${tmin_iso}" +'%s')"


do_touch () {
  mkdir -p "$(dirname "${1}")" \
    && touch "${1}"
}
ls "../podcasts"/*/"${1}" >/dev/null 2>&1 \
  || do_touch "../podcasts/ad_hoc/${1}"
sh "../podcasts/app/marker.sh" "${1}"


rm "${1}.reserved" >/dev/null 2>&1 || true
mkdir -p "$(dirname "${1}.pending")"

readonly dt_s=$((tmin_epo - $(date +'%s') - 0))
if [ 0 -gt $((dt_s)) ] ; then
  readonly dt_m=0
else
  readonly dt_m=$((dt_s / 60))
fi

# how to report stderr to error.log?
echo "sh app/record.sh ${1} 2>&1 | logger" \
| at "now + $((dt_m)) minutes" 2>&1 \
| tee "${1}.pending" \
| logger


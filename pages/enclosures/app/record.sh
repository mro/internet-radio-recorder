#!/bin/sh
# https://mro.name/radio-privatkopie
#

[ "" = "${1}" ] && {
  cat >&2 <<EOF
Record a mp3/aac stream for the given time.

SYNOPSIS

  \$ ${0} <identifier>

EXAMPLE

  \$ ${0} b2/2022/01/21/0830

EOF
  exit 0
}

cd "$(dirname "${0}")/../" || exit 1

[ -r "${1}.ripping" ] && exit 0
ls "${1}".??? >/dev/null 2>&1 && exit 0

readonly log="../error.log"
readonly bc="../stations/${1}.xml"
[ -r "${bc}" ] || {
  echo "file not found: ${bc}" | tee -a "${log}"
  exit 1
}

xml_name () {
  xmllint \
    --nowarning \
    --xpath "string(/*/*[@name='${1}']/@content)" \
    "${2}"
}

readonly tmin_iso="$(xml_name "DC.format.timestart" "${bc}")"
readonly tmin_epo="$(date -d "${tmin_iso}" +'%s')"
readonly tmax_iso="$(xml_name "DC.format.timeend"   "${bc}")"
readonly tmax_epo="$(date -d "${tmax_iso}" +'%s')"

# source before sleeping in order to fail early
readonly sta="$(echo "${1}" | cut -d / -f 1)"
# shellcheck source=../../stations/b2/app/etc.sh
# shellcheck disable=SC1091
. "../stations/${sta}/app/etc.sh"

[ -r "${1}.reserved" ] && rm "${1}.reserved"
readonly delay=$((tmin_epo - $(date +'%s') - 5))
[ 0 -lt $((delay)) ] && {
  echo "... sleep until ${tmin_iso} ..."
  sleep $((delay))
}

readonly dt=$((tmax_epo - $(date +'%s') + 45))
[ 0 -ge $((dt)) ] && {
  echo "failed: invalid max-time $((dt))" | tee -a "${log}"
  exit 1
}
mkdir -p "$(dirname "${1}.ripping")"
[ -r "${1}.pending" ] && rm "${1}.pending"
echo "... ripping ${1} until ${tmax_iso} ..."
curl \
  --location \
  --max-time $((dt)) \
  --output "${1}.ripping" \
  --silent \
  --url "${STREAM_URL}" \
  --user-agent "https://mro.name/radio-privatkopie"

id3v2 \
  --album   "$(xml_name "DC.subject" "${bc}")" \
  --artist  "$(xml_name "DC.author"  "${bc}")" \
  --comment "${1}" \
  --song    "$(xml_name "DC.title"   "${bc}")" \
  "${1}.ripping"

# extension must be 3-char to be picked up by the podcasts etc.
# or even post-process like $ ffmpeg -i "${1}.ripping" "${1}.m4a" && rm "${1}.ripping"
case "$(file --brief --mime-type "${1}.ripping")" in
  'audio/x-hx-aac-adts') ffmpeg -i "${1}.ripping" "${1}.m4a" && rm "${1}.ripping";;
  *)                     mv "${1}.ripping" "${1}.mp3";;
esac
sh "../podcasts/app/publish.sh" "${1}"


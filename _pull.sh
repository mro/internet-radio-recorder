#!/bin/sh
# http://purl.rec.mro/recorder
#
cd "$(dirname "${0}")"

rsync -avPz \
  --exclude enclosures/ --exclude logs/ --exclude stations/ \
  c2:/var/www/vhosts/rec.mro.name/ .

for st in \
  enclosures podcasts \
  stations/b1 stations/b2 stations/b5 stations/brheimat stations/b+ stations/puls stations/ro-galaxy
do
rsync -avPz \
  --exclude _build/ \
  c2:/var/www/vhosts/rec.mro.name/pages/${st}/app pages/${st}/
done

scp c2:/var/www/vhosts/rec.mro.name/bin .
scp c2:/etc/sudoers.d/internet-radio-recorder etc/sudoers.d/
scp c2:/etc/logrotate.d/internet-radio-recorder etc/logrotate.d/

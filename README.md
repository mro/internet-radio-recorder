
# Radio Privatkopie

```
______          _ _        ______     _            _   _               _      
| ___ \        | (_)       | ___ \   (_)          | | | |             (_)     
| |_/ /__ _  __| |_  ___   | |_/ / __ ___   ____ _| |_| | _____  _ __  _  ___ 
|    // _` |/ _` | |/ _ \  |  __/ '__| \ \ / / _` | __| |/ / _ \| '_ \| |/ _ \
| |\ \ (_| | (_| | | (_) | | |  | |  | |\ V / (_| | |_|   < (_) | |_) | |  __/
\_| \_\__,_|\__,_|_|\___/  \_|  |_|  |_| \_/ \__,_|\__|_|\_\___/| .__/|_|\___|
                                                                | |           
                                                                |_|           
```

Automatically record certain radio broadcasts and make rss feeds for private use.

## Install / Update

### Minimum

- download, unpack and run from the commandline `$ sh pages/app/monitor.cgi`
- install dependencies as mentioned, e.g. curl, xsltproc, id3v2, cron, at
- add a dedicated user 'radio-pi' incl. sudo rule
- setup cron to trigger `$ sh pages/app/cron/hourly.sh`

### Recommended

- webserver
- monitoring e.g. via https://updown.io/r/c7a7S

### Optional

- logrotate
- hardening (file owners, permissions)
- cron trigger `$ sh pages/app/cron/weekly.sh`

## Design Goals

| Quality         | very good | good | normal | irrelevant |
|-----------------|:---------:|:----:|:------:|:----------:|
| Functionality   |           |      |    ×   |            |
| Reliability     |           |   ×  |        |            |
| Usability       |           |      |    ×   |            |
| Efficiency      |           |      |    ×   |            |
| Changeability   |     ×     |      |        |            |
| Portability     |           |   ×  |        |            |

## Mirrors

see doap.rdf

## Credits

* https://curl.se/
* http://xmlsoft.org/
* https://github.com/aantron/lambdasoup
* https://github.com/stedolan/jq
* http://id3v2.sourceforge.net/

## Similar

* https://github.com/Wikinaut/MyStreamRecorder
* https://github.com/DirkR/capturadio
* https://github.com/cosmovision/radiorecorder
* https://github.com/prop/radio-recorder
* https://github.com/BenHammersley/RadioPod
* https://github.com/MiczFlor/streamplan (http://www.sourcefabric.org/en/community/blog/2077/)
* http://www.stefan-lehnert.de/wortpresse/?p=2355 (recipe)


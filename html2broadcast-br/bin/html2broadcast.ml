let () =
  let print_help oc =
    let msg =
      "T.b.d.\n\n\
       SYNOPSIS\n\n\
      \  $ ./html2broadcast -h                # get this help\n\
      \  $ ./html2broadcast -V                # version\n\n\
       EXAMPLE\n\n\
      \  $ ./html2broadcast < ausstrahlung-9284673.html\n"
    in
    Printf.fprintf oc "%s\n" msg;
    0
  and print_version oc =
    let exe = Sys.executable_name |> Filename.basename in
    Printf.fprintf oc "%s: https://mro.name/%s/v%s\n" exe
      "broadcast-scrape-br" Version.git_sha;
    0
  in
  (match Sys.argv |> Array.to_list |> List.tl with
   | [] -> Lib.Br.scrape stdin |> Lib.Broadcast.to_xml stdout
   | [ "-V" ] | [ "--version" ] -> print_version stdout
   | [ "-h" ] | [ "--help" ] | _ -> print_help stdout)
  |> exit

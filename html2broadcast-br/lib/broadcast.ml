type timestamp = Ptime.t * int

type t = {
  author : string;
  description : string;
  identifier : string;
  image : string;
  language : string;
  modified : timestamp;
  source : string;
  subject : string;
  timeend : timestamp;
  timestart : timestamp;
  title : string;
  title_episode : string;
  title_series : string;
}

let empty : t =
  let nul = "" and zero = (Ptime.min, 0) in
  {
    author = nul;
    description = nul;
    identifier = nul;
    image = nul;
    language = nul;
    modified = zero;
    source = nul;
    subject = nul;
    timeend = zero;
    timestart = zero;
    title = nul;
    title_episode = nul;
    title_series = nul;
  }

let to_signals bc =
  let dur_iso ((t0, z0), (t1, z1)) =
    let s_per_m = 60 in
    "PT"
    ^ (z0 - z1
       + ((Ptime.diff t1 t0 |> Ptime.Span.to_int_s |> Option.get) / s_per_m)
       |> Int.to_string)
    ^ "M"
  and to_rfc3339 (t, tz_s) = t |> Ptime.to_rfc3339 ~tz_offset_s:tz_s
  and lf l = l |> List.cons (`Text [ "\n" ]) in
  let meta show k v l =
    match show with
    | false -> l
    | true ->
      l |> lf
      |> List.cons (`Text [ "  " ])
      |> List.cons
        (`Start_element
           ( ("", "meta"),
             [ (("", "content"), v); (("", "name"), "DC." ^ k) ] ))
      |> List.cons `End_element
  in
  []
  |> List.cons
    (`Comment
       " unorthodox relative namespace to enable \
        http://www.w3.org/TR/grddl-tests/#sq2 without a central server ")
  |> lf
  |> List.cons
    (`Start_element
       ( ("", "broadcast"),
         [
           (("", "xml:lang"), bc.language);
           (("", "xmlns"), "../../../../../assets/2013/radio-pi.rdf");
           (("", "modified"), bc.modified |> to_rfc3339);
         ] ))
  |> meta true "identifier" bc.identifier
  |> meta true "scheme" "/app/pbmi2003-recmod2012/"
  |> meta true "language" bc.language
  |> meta true "title" bc.title
  |> meta (not (String.equal "" bc.title_series)) "title.series" bc.title_series
  |> meta
    (not (String.equal "" bc.title_episode))
    "title.episode" bc.title_episode
  |> meta true "subject" bc.subject
  |> meta true "format.timestart" (bc.timestart |> to_rfc3339)
  |> meta true "format.timeend" (bc.timeend |> to_rfc3339)
  |> meta true "format.duration" ((bc.timestart, bc.timeend) |> dur_iso)
  |> meta true "image" bc.image
  |> meta true "description" bc.description
  |> meta true "author" bc.author
  |> meta false "creator" "-" |> meta false "publisher" "-"
  |> meta true "source" bc.source
  |> lf
  |> List.cons `End_element
  |> lf |> List.rev

let to_xml dst bc =
  bc |> to_signals |> Markup.of_list |> Markup.write_xml
  |> Markup.to_channel dst;
  0

(* http://aantron.github.io/lambdasoup/ *)
open Soup

let test_soup () =
  let soup = "data/ausstrahlung-2601832.html" |> read_file |> parse in
  assert (
    "radioWelt"
    = (soup $ "html > head > meta[name=DCTERMS.title]" |> R.attribute "content"));
  assert (
    [ "Dienstag, 14.09.2021"; "06:05\nbis 08:30 Uhr" ]
    = (soup $ ".bcast_date" |> trimmed_texts));
  assert (
    "\nDienstag, 14.09.2021    06:05\nbis 08:30 Uhr\n"
    = (soup $ ".bcast_date" |> texts |> String.concat " "))

let test_date () =
  let tz = Timedesc.Time_zone.make_exn "Europe/Zurich"
  and s = "Dienstag, 14.09.2021 06:05 bis 08:30 Uhr"
  and rx =
    Re.Pcre.regexp
      "([0-9]{2})\\.([0-9]{2})\\.([0-9]{4})[^0-9]+([0-9]{2}:[0-9]{2})[^0-9]+([0-9]{2}:[0-9]{2})"
  in
  let m = Re.exec rx s in
  assert (6 = Re.Group.nb_groups m);
  let g idx = Re.Group.get m idx in
  let d = Timedesc.Date.of_iso8601_exn (g 3 ^ "-" ^ g 2 ^ "-" ^ g 1)
  and t = Timedesc.Time.of_iso8601_exn (g 4) in
  let zl = Timedesc.Zoneless.of_date_and_time d t in
  let zo = Result.get_ok (Timedesc.Zoneless.to_zoned ~tz zl) in
  assert ("2021-09-14T06:05:00+02:00" = (Timedesc.to_rfc3339 zo));
  zo
  |> Timedesc.to_rfc3339
  |> Assert2.equals_string "b2.date 10" "2021-09-14T06:05:00+02:00" 

let test_make_identifier () =
  let of_rfc3339 str =
    match str |> Ptime.of_rfc3339 with
    | Ok (t, Some tz_s, _) -> (t, tz_s)
    | _ -> (Ptime.min, 0)
  in
  let t0 =
    ( ((2021, 9, 22), ((8, 30, 0), 7200)) |> Ptime.of_date_time |> Option.get,
      7200 )
  and _ = "2021-09-22T08:30:00+02:00" |> of_rfc3339 in
  let to_id src = Lib.Br.make_identifier src t0 in
  "https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2601832.html"
  |> to_id
  |> Assert2.equals_string "b2.make_identifier 10" "b2/2021/09/22/0830"

let test_scrape_ausstrahlung_2601832 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2601832.html" |> read_file) in
  assert ("Bayerischer Rundfunk" = bc.author);
  (* Printf.printf "%s\n" bc.description; *)
  assert (
    "Magazin am Morgen Kurz erklärt:  So geriet Joachim Treiber ins Fadenkreuz \
     rechten Terrors Gespräch mit  Joachim Treiber, Caritas Stuttgart: \
     Ausstellung \"Menschen - Im Fadenkreuz rechten Terrors\"   Gespräch mit  \
     Konstanze von Unold, Grundschulleiterin: erster Schultag im dritten \
     Corona-Schuljahr Zur Person:  Peter Boehringer Gespräch mit  Peter \
     Boehringer, Spitzenkandidat der AfD in Bayern für die Bundestagswahl \
     Schauspielerin Martina Gedeck wird 60 - ein Porträt Die Presse  mit \
     Wolfram Schrag Gedanken zum Tag  mit Uwe Böschemeyer Ende der Welt  von \
     Georg Bayerle 6.30 / 7.30 Kurznachrichten, Wetter, Verkehr 7.00 / 8.00 \
     Nachrichten, Wetter, Verkehr 7.28 Werbung Ausgewählte Beiträge und \
     Interviews als Podcast und in der neuen Bayern 2 App verfügbar"
    = bc.description);
  assert ("b2/2021/09/14/0605" = bc.identifier);
  assert (
    "https://www.br.de/radio/bayern2/sendungen/radiowelt/moderation/stefan-kreutzer108~_v-img__16__9__xl_-d31c35f8186ebeb80b0cd843a7c267a0e0c81647.jpg?version=d4047"
    = bc.image);
  assert ("de" = bc.language);
  assert (
    "https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2601832.html"
    = bc.source);
  assert (
    "https://www.br.de/radio/bayern2/sendungen/radiowelt/index.html"
    = bc.subject);
  assert ("radioWelt" = bc.title);
  (* Printf.printf "%s\n" bc.title_episode; *)
  assert ("Moderation: Stefan Kreutzer" = bc.title_episode)

let test_scrape_ausstrahlung_2609146 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2609146.html" |> read_file) in
  assert ("b2/2021/09/24/1205" = bc.identifier);
  assert (
    "https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2609146.html"
    = bc.source);
  assert (
    "https://www.br.de/radio/bayern2/sendungen/tagesgespraech/index.html"
    = bc.subject);
  assert ("Tagesgespräch" = bc.title);
  (* Printf.printf "%s\n" bc.title_episode; *)
  assert (
    "70 Jahre Bundesverfassungsgericht: Welche Urteile waren besonders \
     wegweisend?" = bc.title_episode)

let test_scrape_ausstrahlung_2609972 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2609972.html" |> read_file) in
  assert ("Bayerischer Rundfunk" = bc.author);
  assert ("" = bc.title_episode)

let test_scrape_ausstrahlung_2611380 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2611380.html" |> read_file) in
  assert ("Impressum" = bc.title);
  assert ("" = bc.subject);
  assert ("" = bc.title_episode)

let test_scrape_ausstrahlung_2611384 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2611384.html" |> read_file) in
  assert ("BR-Heimatspiegel" = bc.title);
  assert (
    "https://www.br.de/radio/br-heimat/sendungen/heimatspiegel/index.html"
    = bc.subject);
  assert ("Mit Volksmusik gut in den Tag" = bc.title_episode)

let test_scrape_ausstrahlung_2611450 () =
  let dur_iso ((t0, z0), (t1, z1)) =
    let s_per_m = 60 in
    "PT"
    ^ (z0 - z1
       + ((Ptime.diff t1 t0 |> Ptime.Span.to_int_s |> Option.get) / s_per_m)
       |> Int.to_string)
    ^ "M"
  and bc = Lib.Br.scrape_str ("data/ausstrahlung-2611450.html" |> read_file) in
  assert ("PT55M" = dur_iso (bc.timestart, bc.timeend));
  assert ("Mit Sabine Gietzelt" = bc.title_episode)

let test_scrape_ausstrahlung_2612988 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2612988.html" |> read_file) in
  (* Printf.printf "%s\n" bc.subject; *)
  assert (
    "https://www.br-klassik.de/programm/sendungen-a-z/ard-nachtkonzert-100.html"
    = bc.subject)

let test_scrape_ausstrahlung_2645204 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2645204.html" |> read_file) in
  (* Printf.printf "%s\n" bc.identifier; *)
  assert ("b5/2021/10/04/1813" = bc.identifier)

let test_scrape_ausstrahlung_2647972 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2647972.html" |> read_file) in
  (*
  Printf.printf "title: %s\n" bc.title;
  Printf.printf "episode: %s\n" bc.title_episode;
  Printf.printf "series: %s\n" bc.title_series;
  Printf.printf "id: %s\n" bc.identifier;
  *)
  assert ("Infoblock" = bc.title);
  assert (
    "Werbung nur zwischen 6.00 und 9.00, 12.00 und 13.00 sowie 17.00 und 18.00 \
     Uhr" = bc.title_series);
  assert ("" = bc.title_episode);
  assert ("b5/2021/10/07/0600" = bc.identifier)

let test_scrape_ausstrahlung_2651390 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2651390.html" |> read_file) in
  (* Printf.printf "%s\n" bc.identifier; *)
  assert ("puls/2021/10/12/0600" = bc.identifier)

let test_scrape_ausstrahlung_2624044 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2624044.html" |> read_file) in
  (* Printf.printf "%s\n" bc.identifier; *)
  assert ("b2/2021/10/23/0003" = bc.identifier)

let test_scrape_ausstrahlung_2690002 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2690002.html" |> read_file) in
  (* Printf.printf "%s\n" bc.title_episode; *)
  assert (
    "Weihnachtsansprache und Segen \"Urbi et Orbi\" von Papst Franziskus"
    = bc.title);
  assert ("Live vom Petersplatz in Rom" = bc.title_series);
  assert ("" = bc.title_episode);
  assert ("b1/2021/12/25/1200" = bc.identifier)

let test_scrape_ausstrahlung_2648536 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2648536.html" |> read_file) in
  (* Printf.printf "%s\n" bc.title_episode; *)
  assert ("radioWelt" = bc.title);
  assert ("" = bc.title_series);
  assert ("Moderation: Franziska Eder" = bc.title_episode);
  assert ("b2/2021/11/15/1705" = bc.identifier)

let test_scrape_ausstrahlung_2725596 () =
  let bc = Lib.Br.scrape_str ("data/ausstrahlung-2725596.html" |> read_file) in
  (* Printf.printf "%s\n" bc.identifier; *)
  assert ("Der BR Schlager Treff" = bc.title);
  assert ("" = bc.title_series);
  assert ("" = bc.title_episode);
  assert ("b+/2021/11/22/1005" = bc.identifier)

let test_scrape_ausstrahlung_2742652 () =
  let bc = "data/ausstrahlung-2742652.html" |> read_file |> Lib.Br.scrape_str in
  Assert2.equals_string "tit" "Nachrichten, Wetter" bc.title;
  Assert2.equals_string "id" "b2/2022/02/06/0200" bc.identifier

let test_scrape_ausstrahlung_2807178 () =
  let bc = "data/ausstrahlung-2807178.html" |> read_file |> Lib.Br.scrape_str in
  Assert2.equals_string "tit" "Der BR Schlager Treff" bc.title;
  Assert2.equals_string "id" "b+/2022/03/24/1005" bc.identifier

(*
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742652.html
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742656.html
https://br.de//radio/br-schlager/programmkalender/ausstrahlung-2742774.html
https://br.de//radio/br-schlager/programmkalender/ausstrahlung-2742772.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768756.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768754.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768764.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768760.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768762.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768758.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768778.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768766.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768770.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768756.html
https://br.de//radio/bayern1/programmkalender/ausstrahlung-2742610.html
https://br.de//radio/br-schlager/programmkalender/ausstrahlung-2742772.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768794.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768790.html
https://br.de//radio/br24/programmkalender/ausstrahlung-2768788.html
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742672.html
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742674.html
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742678.html
https://br.de//radio/br-heimat/programmkalender/ausstrahlung-2729662.html
https://br.de//radio/br-heimat/programmkalender/ausstrahlung-2729672.html
https://br.de//radio/bayern2/programmkalender/ausstrahlung-2742684.html
https://br.de//puls/programm/puls-radio/programmkalender/ausstrahlung-2762402.html
*)

let test_foo () = assert true

let () =
  Unix.chdir "../../../test/";
  test_soup ();
  test_date ();
  test_make_identifier ();
  test_scrape_ausstrahlung_2601832 ();
  test_scrape_ausstrahlung_2609146 ();
  test_scrape_ausstrahlung_2609972 ();
  test_scrape_ausstrahlung_2611380 ();
  test_scrape_ausstrahlung_2611384 ();
  test_scrape_ausstrahlung_2611450 ();
  test_scrape_ausstrahlung_2612988 ();
  test_scrape_ausstrahlung_2645204 ();
  test_scrape_ausstrahlung_2647972 ();
  test_scrape_ausstrahlung_2651390 ();
  test_scrape_ausstrahlung_2624044 ();
  test_scrape_ausstrahlung_2690002 ();
  test_scrape_ausstrahlung_2648536 ();
  test_scrape_ausstrahlung_2725596 ();
  test_scrape_ausstrahlung_2742652 ();
  test_scrape_ausstrahlung_2807178 ();
  test_foo ()

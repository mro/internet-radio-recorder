let test_markup () =
  let s =
    [
      `Start_element (("bar", "foo"), [ (("", "bar"), "baz") ]);
      `Text [ "more"; " "; "text" ];
      `End_element;
    ]
    |> Markup.of_list |> Markup.write_xml |> Markup.to_string
  in
  assert (s = "<foo bar=\"baz\">more text</foo>")

let _test_markup2 () =
  let lf = `Text [ "\n" ]
  and lfi = `Text [ "\n  " ]
  and meta k v =
    `Start_element (("", "meta"), [ (("", "content"), v); (("", "name"), k) ])
  and en = `End_element in
  let s =
    [
      `PI
        ( "xml-stylesheet",
          "type='text/xsl' href='../../../app/broadcast2html.xslt'" );
      lf;
      `Start_element
        ( ("baf", "broadcast"),
          [
            (("", "xml:lang"), "de");
            (("", "xmlns"), "../../../../../assets/2013/radio-pi.rdf");
            (("", "modified"), "2021-08-13T08:05:22+02:00");
          ] );
      lfi;
      meta "DC.identifier" "b2/2021/09/27/0200";
      en;
      lfi;
      meta "DC.scheme" "/app/pbmi2003-recmod2012/";
      en;
      lfi;
      meta "DC.language" "de";
      en;
      lfi;
      meta "DC.title" "Nachrichten, Wetter";
      en;
      lfi;
      meta "DC.subject" "https://www.br.de/nachrichten/meldungen/index.html";
      en;
      lfi;
      meta "DC.timestart" "2021-09-27T02:00:00+02:00";
      en;
      lfi;
      meta "DC.timeend" "2021-09-27T02:03:00+02:00";
      en;
      lfi;
      meta "DC.duration" "PT3M";
      en;
      lfi;
      meta "DC.image"
        "https://www.br.de/radio/br24-programmfahne-nachrichten-100~_v-img__16__9__xl_-d31c35f8186ebeb80b0cd843a7c267a0e0c81647.jpg?version=e37d8";
      en;
      lfi;
      meta "DC.description"
        "Die aktuellen Nachrichten des Bayerischen Rundfunks - auch hier auf \
         BR.de zum Nachlesen .";
      en;
      lfi;
      meta "DC.author" "Bayerischer Rundfunk";
      en;
      lfi;
      meta "DC.source"
        "https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2610734.html";
      en;
      lf;
      `End_element;
    ]
    |> Markup.of_list |> Markup.write_xml |> Markup.to_string
  in
  Printf.printf "%s\n" s;
  assert (
    "<?xml-stylesheet type='text/xsl' href='../../../app/broadcast2html.xslt'?>\n\
     <broadcast xml:lang=\"de\" \
     xmlns=\"../../../../../assets/2013/radio-pi.rdf\"modified=\"2021-08-13T08:05:22+02:00\">\n\
    \  <meta content=\"b2/2021/09/27/0200\" name=\"DC.identifier\"/>\n\
    \  <meta content=\"/app/pbmi2003-recmod2012/\" name=\"DC.scheme\"/>\n\
    \  <meta content=\"de\" name=\"DC.language\"/>\n\
    \  <meta content=\"Nachrichten, Wetter\" name=\"DC.title\"/>\n\
    \  <meta content=\"https://www.br.de/nachrichten/meldungen/index.html\" \
     name=\"DC.subject\"/>\n\
    \  <meta content=\"2021-09-27T02:00:00+02:00\" name=\"DC.timestart\"/>\n\
    \  <meta content=\"2021-09-27T02:03:00+02:00\" name=\"DC.timeend\"/>\n\
    \  <meta content=\"PT3M\" name=\"DC.duration\"/>\n\
    \  <meta \
     content=\"https://www.br.de/radio/br24-programmfahne-nachrichten-100~_v-img__16__9__xl_-d31c35f8186ebeb80b0cd843a7c267a0e0c81647.jpg?version=e37d8\" \
     name=\"DC.image\"/>\n\
    \  <meta content=\"Die aktuellen Nachrichten des Bayerischen Rundfunks - \
     auch hier auf BR.de zum Nachlesen.\" name=\"DC.description\"/>\n\
    \  <meta content=\"Bayerischer Rundfunk\" name=\"DC.author\"/>\n\
    \  <meta \
     content=\"https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2610734.html\" \
     name=\"DC.source\"/>\n\
     </broadcast>" = s)

let test_markup3 () =
  let meta k v l =
    l
    |> List.cons `End_element
    |> List.cons
      (`Start_element
         (("", "meta"), [ (("", "content"), v); (("", "name"), k) ]))
    |> List.cons (`Text [ "\n  " ])
  in
  let l =
    []
    |> List.cons
      (`PI
         ( "xml-stylesheet",
           "type='text/xsl' href='../../../app/broadcast2html.xslt'" ))
    |> List.cons (`Text [ "\n" ])
    |> List.cons
      (`Start_element
         ( ("baf", "broadcast"),
           [
             (("", "xml:lang"), "de");
             (("", "xmlns"), "../../../../../assets/2013/radio-pi.rdf");
             (("", "modified"), "2021-08-13T08:05:22+02:00");
           ] ))
    |> meta "DC.identifier" "b2/2021/09/27/0200"
    |> meta "DC.scheme" "/app/pbmi2003-recmod2012/"
    |> meta "DC.language" "de"
    |> meta "DC.title" "Nachrichten, Wetter"
    |> meta "DC.subject" "https://www.br.de/nachrichten/meldungen/index.html"
    |> meta "DC.timestart" "2021-09-27T02:00:00+02:00"
    |> meta "DC.timeend" "2021-09-27T02:03:00+02:00"
    |> meta "DC.duration" "PT3M"
    |> meta "DC.image"
      "https://www.br.de/radio/br24-programmfahne-nachrichten-100~_v-img__16__9__xl_-d31c35f8186ebeb80b0cd843a7c267a0e0c81647.jpg?version=e37d8"
    |> meta "DC.description"
      "Die aktuellen Nachrichten des Bayerischen Rundfunks - auch hier auf \
       BR.de zum Nachlesen ."
    |> meta "DC.author" "Bayerischer Rundfunk"
    |> meta "DC.source"
      "https://www.br.de/radio/bayern2/programmkalender/ausstrahlung-2610734.html"
    |> List.cons `End_element
    |> List.rev
  in
  assert (3 + (12 * 3) + 1 = (l |> List.length))

let test_foo () = assert true

let () =
  test_markup ();
  (* test_markup2 (); *)
  test_markup3 ();
  test_foo ()

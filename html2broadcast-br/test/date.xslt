<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes=""
  xmlns:date="http://exslt.org/dates-and-times"
  extension-element-prefixes="date"
  version="1.0">

  <xsl:output method="text"/>

  <xsl:template match="/">
    <xsl:variable name="pat" select="'~_date-'"/>
    <xsl:variable name="tom_href" select="//*[@class='multidays_next']//a[contains(@href, $pat)]/@href"/>
    <xsl:variable name="tomorrow" select="substring(substring-after($tom_href, $pat), 1, 10)"/>
    <!-- http://exslt.org/date/functions/add/ -->
    <xsl:variable name="today" select="date:add($tomorrow, '-P1D')"/>
    _ <xsl:value-of select="$today"/>
    _ <xsl:value-of select="$tomorrow"/>
    _ <xsl:value-of select="date:day-abbreviation($tomorrow)"/>

  </xsl:template>
</xsl:stylesheet>

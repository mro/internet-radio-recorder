let test_re () =
  let s = "Dienstag, 14.09.2021 06:05 bis 08:30 Uhr" in
  let rx =
    Re.Pcre.regexp
      "([0-9]{2})\\.([0-9]{2})\\.([0-9]{4})[^0-9]+([0-9]{2}:[0-9]{2})[^0-9]+([0-9]{2}:[0-9]{2})"
  in
  let m = Re.exec rx s in
  assert (6 = Re.Group.nb_groups m);
  let g idx = Re.Group.get m idx in
  assert ("2021-09-14T06:05" = g 3 ^ "-" ^ g 2 ^ "-" ^ g 1 ^ "T" ^ g 4);
  assert ("2021-09-14T08:30" = g 3 ^ "-" ^ g 2 ^ "-" ^ g 1 ^ "T" ^ g 5)

let () = test_re ()

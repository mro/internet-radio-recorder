let test_ptime () =
  (* How would I get the timezone offset for a given Timezone (e.g. local or
     Europe/Amsterdam at a given datetime? *)
  let str0 = "2021-09-28T09:25:00+02:00" in
  match Ptime.of_rfc3339 str0 with
  | Ok (t0, Some tz_s, 25) -> (
      assert (str0 = Ptime.to_rfc3339 ~tz_offset_s:tz_s t0);
      match Ptime.of_rfc3339 "2021-09-28T09:37:00+02:00" with
      | Ok (t1, Some 7200, 25) ->
        let dt = Ptime.to_float_s t1 -. Ptime.to_float_s t0 in
        assert (12.0 *. 60.0 = dt)
      | _ -> assert false)
  | _ -> assert false

let test_timedesc () =
  (* let tzV = Option.get (Timedesc.Time_zone.local ()) in
     assert ("Europe/Vienna" = Timedesc.Time_zone.name tzV); *)
  let tz = Timedesc.Time_zone.make_exn "Europe/Zurich" in
  assert ("Europe/Zurich" = Timedesc.Time_zone.name tz);
  let t0 =
    Timedesc.make_exn ~tz ~year:2021 ~month:9 ~day:21 ~hour:9 ~minute:11
      ~second:0 ()
  and t1 =
    Timedesc.make_exn ~tz ~year:2021 ~month:9 ~day:21 ~hour:9 ~minute:17
      ~second:0 ()
  in
  t0 |> Timedesc.to_rfc3339 |> Assert2.equals_string "uu 100" "2021-09-21T09:11:00+02:00";
  assert ("2021-09-21T09:17:00+02:00" = (Timedesc.to_rfc3339 t1));
  let dt =
    Timedesc.to_timestamp_float_s_single t1
    -. Timedesc.to_timestamp_float_s_single t0
  in
  assert (6.0 *. 60.0 = dt)

let test_zoneless_to_zoned () =
  let tz = Timedesc.Time_zone.make_exn "Europe/Zurich"
  and t = Timedesc.Time.make_exn ~hour:1 ~minute:2 ~second:3 ()
  and d0 = Timedesc.Date.of_iso8601_exn "2021-09-28" in
  let zl0 = Timedesc.Zoneless.of_date_and_time d0 t in
  let zo0 = Result.get_ok (Timedesc.Zoneless.to_zoned ~tz zl0) in
  assert ("2021-09-28T01:02:03+02:00" = (Timedesc.to_rfc3339 zo0));
  let d1 = Timedesc.Date.of_iso8601_exn "2021-12-28" in
  let zl1 = Timedesc.Zoneless.of_date_and_time d1 t in
  let zo1 = Result.get_ok (Timedesc.Zoneless.to_zoned ~tz zl1) in
  assert ("2021-12-28T01:02:03+01:00" = (Timedesc.to_rfc3339 zo1))

let test_foo () = assert true

let () =
  test_ptime ();
  test_timedesc ();
  test_zoneless_to_zoned ();
  test_foo ()
